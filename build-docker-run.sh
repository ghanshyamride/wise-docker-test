#bin/bash
#build the artifact
./mvnw clean package
#build docker image
docker build -t ghanshyamkolse/demo-spring-boot .
#run docker image
docker run -p 8081:8080 ghanshyamkolse/demo-spring-boot